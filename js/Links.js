/**
 * Link clicking manager, monitors all links and gets correct attributes.
 * @plugin {Sniff} Sniff: you need to pass in a Sniff object
 *
 * @constructor
 * */
function Links() {
    this.queries;
    this.name;
    this.initiated = false;
    
    /**
     * Finds out if an event target is a link for LMC or external
     * @param {Event} event
     * @return {Links}
     * */
    this.convert = function (event) {
        if (event.target) {
            var lmc = "lausanne-montreux-congress";
            var link = event.target;
            
            // Todo: @Lauric: ajoute une entrée dans le fichier assets_cms -> isProd : "{{ mycity_is_production() == false ? "false" : "true" }}",
            if (CMS.isProd === "false") lmc = location.host;
            
            // moving up until we meet an anchor link
            var max = 0; // maximum up level to go
            while (link.localName !== "a" && max < 4) {
                link = link.parentElement;
                max++;
            }
            
            if (link.href.indexOf(lmc) > -1) {
                // menu clicking or void links clicking, let's not do anything
                if (link.href.indexOf("#") > -1) {
                    event.preventDefault();
                    return this;
                }
                
                // Special link added by Nav.js to return to LT/MVT, interrupt Links and let the link fire normally
                if (link.classList.contains("backToHome")) {
                    return this;
                }
                
                // now we're sure it's a LMC link
                event.preventDefault();
                
                // checking if link has a data-id and data-locale, otherwise default to options
                this.queries = {
                    id: link.dataset.id ? link.dataset.id : sniff.options.id,
                    locale: link.dataset.locale ? link.dataset.locale : sniff.options.locale
                };
                
                // plugin integration
                if (sniff.navigation !== undefined) {
                    sniff.navigation.prepare({locale: sniff.options.locale, id: sniff.routes.current.id}).commit();
                }
                
                // boom, load the new page
                sniff.update({
                    id: this.queries.id,
                    locale: this.queries.locale
                });
                
                return this;
            }
        }
    };
    
    /**
     * "Constructor" function
     * @return {string}
     * */
    this.init = function () {
        if (!this instanceof Sniff) {
            return console.error("Error: Was not registered as a Sniff plugin.");
        }
        
        var self = this;
        
        if (this.initiated === false) {
            sniff.container.addEventListener("click", self.convert.bind(self), false);
            this.initiated = true;
        }
        
        return this.name = "links";
    };
    
    this.init();
}