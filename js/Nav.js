function Nav() {
    
    this.nav = {
        offcanvas: document.getElementById("offcanvas"),
        burger: document.querySelector(".nav-burger"),
        logo: document.querySelector('.nav-logo'),
        nav: document.querySelector(".lmc-nav")
    };
    
    this.init = function () {
        document.addEventListener("click", this.showMenu.bind(this), false);
        this.backToHome();
    };
    
    this.showMenu = function (event) {
        if (event.target) {
            // clicked on "close" button
            if (event.target.classList.contains("close")) {
                event.preventDefault();
            }
            // clicked elsewhere
            if (event.target.classList.contains("navigation")) {
                // clicked on burger
                event.preventDefault();
                this.nav.offcanvas.classList.add("menu-open");
                if (this.nav.logo) this.nav.logo.style.zIndex = 0;
            } else {
                // clicked on anything
                this.nav.offcanvas.classList.remove("menu-open");
                if (this.nav.logo) this.nav.logo.style.zIndex = '';
            }
        }
    };
    
    this.backToHome = function () {
        var ul = this.nav.offcanvas.querySelector('ul');
        var li = ul.querySelector('li.backToHome');
        
        if (li === null) {
            li = document.createElement('li');
            li.classList.add('backToHome');
            var a = document.createElement('a');
            a.classList.add('backToHome');
            a.href = window.location.origin + "/" + CMS.locale;
            a.innerText = "Back to Lausanne Tourisme";
            
            li.appendChild(a);
            ul.appendChild(li);
        }
        
        return this;
    };
    
    this.init();
}

//var nav = new Nav(); // --> stops making some of the errors :D