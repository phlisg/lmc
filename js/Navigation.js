/**
 * History manager plugin. Works like "Ctrl-Z": an array stores current page and increments itself to store the next page. On "previous",
 * it retrieves last page and then rewrites future routes
 * @plugin {Sniff} sniff -> only usable as a plugin. To register before using sniff!
 *
 * @constructor
 */
function Navigation() {
    this.name;
    this.history = [];
    this.steps = 0;
    this.initiated = false;
    
    // function here so it refreshes the element through page changes | need to look if it induces memory leaks
    this.button = function () {
        return {previous: document.getElementById("previousPage")};
    };
    
    /**
     * The "Previous" mechanism - reads the history and gives back the id/locale of previous page
     * @param event
     * @return {Boolean}
     * */
    this.previous = function (event) {
        event.preventDefault();
        
        console.log(sniff.navigation.steps);
        
        if (sniff.navigation.steps > 0) {
            sniff.update(sniff.navigation.history[--sniff.navigation.steps]);
            
            return true;
        }
        
        sniff.navigation._activeButton();
        
        return false;
    };
    
    /**
     * Capturing current data before page change
     * @param {Object} data : { id, locale }
     * */
    this.catch = function (data) {
        this.history.push(data);
        this.steps++;
    };
    
    /**
     * Prepare current history state (allows updating fields one by one)
     * @param {Object} data : { id, locale }
     * @return {Navigation}
     * */
    this.prepare = function (data) {
        var temp = {};
        
        for (var value in data) {
            if (data.hasOwnProperty(value)) {
                temp[value] = data[value];
            }
        }
        
        this.history[this.steps] = temp;
        
        return this;
    };
    
    /**
     * Increments the "history" pointer
     * @return {Array} history
     * */
    this.commit = function () {
        this.steps++;
        
        return this.history;
    };
    
    /**
     * Manages event listeners on back button on "POI" templates
     * @return {Navigation}
     * */
    this.listen = function () {
        var sniff = this;
        if (this.button().previous !== null) {
            this.button().previous.removeEventListener("click", sniff.previous, false); // preventing memory leaks?
            this.button().previous.addEventListener("click", sniff.previous.bind(sniff), false);
        }
        
        return this;
    };
    
    /**
     * Manages visual "state" of the back button
     * @return {*}
     * */
    this._activeButton = function () {
        if (this.steps <= 1) {
            if (this.button().previous !== null) {
                return this.button().previous
                    .style.cssText = "background: #f9f9fa; color: #ccc;";
            }
            return this;
        }
        
        if (this.button().previous !== null) {
            return this.button().previous
                .style.cssText = "";
        }
        return this;
    };
    
    /**
     * "Constructor" function
     * @return {string}
     * */
    this.init = function () {
        if (!this instanceof Sniff) {
            return console.error("Error: Was not registered as a Sniff plugin.");
        }
        
        this._activeButton();
        
        if (this.initiated === false) {
            this.listen();
            this.initiated = true;
        }
        
        return this.name = "navigation";
    };
    
    this.init();
}