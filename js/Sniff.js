/*
 * Polyfill to enable forEach support in Edge/IE
 * fixme: need to call it once instead of multiple times from different files...
 * */

(function () {
    if (typeof NodeList.prototype.forEach === "function") return false;
    NodeList.prototype.forEach = Array.prototype.forEach;
})();

/**
 * @constructor
 * @param {string} container : a string containing a CSS-style selector (#content...)
 * */
function Sniff(container, options) {
    this.container = document.querySelector(container);
    this.routes;
    this.css;
    this.html;
    this.timeoutTime = 50;
    this.options = options;
    this.siteName = " - Lausanne-Montreux Congress";
    this.status = {
        styles: false,
        javascript: false
    };
    
    /**
     * @param {Object} options
     * @key {string} locale : "en" (CMS.locale)
     * @key {number} id : Z6507 (charger la page)
     * @key {string} selector : ".myclass element #id" (querySelector used here)
     * @key {Array<string>} callbacks : array of string containing existing callbacks to call after _loading (reactivate JS)
     * @key {Array<string>} elemsToRemove : array of string containing css-style selectors to remove elements
     * @key {function} onfinish : some final closure to operate on obtained data (doing some jQuery stuff for example)
     * */
    this.fetch = function (options) {
        
        // setting defaults if keys are not present (better than undefined! :p)
        if (this.options === undefined) {
            options = {
                locale: options.locale === undefined ? CMS.locale : options.locale,
                id: options.id === undefined ? this.setup() : String(options.id), // just in case ;)
                selector: options.selector === undefined ? null : options.selector,
                callbacks: options.callbacks === undefined ? null : options.callbacks,
                elemsToRemove: options.elemsToRemove === undefined ? null : options.elemsToRemove,
                onfinish: options.onfinish === undefined ? this.onfinish() : options.onfinish
            };
        } else {
            options = this.options;
        }
        
        // Pour pouvoir utiliser l'objet courant dans le callback de jquery.success()
        var self = this;
        
        this._loading("on");
        
        $.ajax({
            /* @Lauric: ajoute une entrée dans assets_cms: lmc : { api: "{{ mycity_link_to(data-controller) }}" */
            url: CMS.lmc.api,
            type: "GET",
            dataType: "json", // parses json directly
            data: {"locale": options.locale, "id": options.id, "format": "json"},
            success: function (data) {
                self._update(self._parseData(data, options.selector), options.callbacks);
                self._parseCss();
                self._parseJson();
                self._cleanup(options.elemsToRemove); // _loading('off') here
                self._updateUrl();
                
                options.onfinish();
                
                if (self.navigation !== undefined) {
                    setTimeout(function () {
                        self.navigation.listen();
                    }, self.timeoutTime);
                }
                
            },
            error: function (error) {
                console.error(error);
            }
        });
        
        return this.options = options;
    };
    
    /**
     * @param {string} html
     * @param {string} selector
     * @return {string}
     * */
    this._parseData = function (data, selector) {
        var doc = document.createRange().createContextualFragment(data.html);
        
        var json = doc.querySelector("#json");
        var css = doc.querySelector("#styles");
        
        // need to find json here
        if (json === null) {
            return console.error("No Json data in <div#json> has been found in remote document.");
        }
        
        if (css === null) {
            return console.error("No styles in <link#styles> were found in remote document. Did you assign an id to your main css <link> element from remote page?");
        }
        
        this._json = json.innerHTML.trim();
        this.css = {
            link: css.href,
            version: css.href.split("/").pop().replace(/\?.+$/ig, "")
        };
        
        if (doc.querySelector(selector) !== null) {
            return doc.querySelector(selector).innerHTML;
        }
        
        console.warn("No selector has been found or was provided, the whole data is sent.");
        
        return doc.innerHTML; // defaulting to the whole html - not recommended!
    };
    
    /**
     * Get remote CSS and replace current css with that one.
     * */
    this._parseCss = function () {
        if (this.css.link !== undefined) {
            
            // do the parsing
            // fixme @Lauric: identify in MVT/LT template the stylesheet to replace -> add id#styles to layout.html.twig
            var oldCss = document.querySelector("#styles");
            
            /**
             * Cleaning up the mess
             * We're finding all CSS and remove unwanted ones. MyCity CSS for sites end up with ?627814
             */
            document.querySelectorAll("head link[rel=\"stylesheet\"]:not(#styles)").forEach(function (style) {
                var match = style.href.match(/\?\d+$/g);
                if (match !== null) {
                    style.parentElement.removeChild(style); // self-deletion, nom nom
                }
            });
            
            // comparing old css name with new version's name
            if (oldCss.href.split("/").pop().replace(/\?.+$/ig, "") !== this.css.version) {
                var link = document.createElement("link");
                
                link.href = this.css.link;
                link.rel = "stylesheet";
                link.type = "text/css";
                link.id = "styles";
                
                document.querySelector("head").appendChild(link);
                oldCss.parentElement.removeChild(oldCss);
                
                this.status.styles = true;
            }
        }
        
        return this;
    };
    
    /**
     * @return {Object} json : converted from Json to JS
     * */
    this._parseJson = function () {
        // convert from raw json to JS
        if (this.routes === undefined && typeof this._json === "string") {
            return this.routes = JSON.parse(this._json);
        } else {
            // implementing basic previous route for in-screen navigation
            return this.routes.previous = {id: this.routes.current.id, locale: this.options.locale};
        }
        
        console.error("No json data found. Are you sure you exported it?");
    };
    
    /**
     * Execute functions post-loading the ajax query
     * @param {Object} data
     * @param {array<String>} callbacks : functions to execute after XHR is done loading
     * @return {*}
     * */
    this._update = function (data, callbacks) {
        
        if (this.container === null) {
            return console.error("Container is null: " + this.container);
        }
        
        // allows execution of scripts post-loading (like re-enabling sliders, widgets etc...)
        if (typeof callbacks !== "undefined" && callbacks instanceof Array) {
            if (window.requestIdleCallback) {
                requestIdleCallback(function () {
                    callbacks.forEach(function (callback) {
                        window[callback]();
                    });
                });
            } else {
                setTimeout(function () {
                    callbacks.forEach(function (callback) {
                        window[callback]();
                    });
                }, this.timeoutTime);
            }
        }
        
        return this.container.innerHTML = data;
    };
    
    /**
     * Removes unwanted elements from XHR document
     * @param {array<String>} elements : any valid CSS selector
     * @return {number}
     * */
    this._cleanup = function (elements) {
        var self = this;
        
        return setTimeout(function () {
            elements.forEach(function (element) {
                // compatible way:
                var child = document.querySelector(element);
                
                if (child !== null) {
                    child.parentElement.removeChild(child); // JS savageness : Mum? can you delete me? But of course darling. *brutally murdered*
                }
            });
            
            this._loading("off");
            
        }.bind(self), this.timeoutTime + 5);
    };
    
    /**
     * Adds a loading screen between page calls
     * @param {string|number|boolean} onOrOff ("on"|"off")
     * @return {boolean}
     * */
    this._loading = function (onOrOff) {
        
        var loading = document.getElementById("loading");
        /* @Lauric: mon loading sur LT est différent que sur MVT, il faudra probablement
         * changer quelques règles CSS pour que ça fonctionne bien
         */
        if (loading === null) {
            var div = document.createElement("div");
            div.id = "loading";
            div.innerHTML = "<div class=\"spinner\"></div>";
            
            document.body.insertBefore(div, document.body.children[0]);
            
            loading = document.getElementById("loading");
        }
        
        if (onOrOff === "on" || onOrOff === 1 || onOrOff === true) {
            loading.style.cssText = "left:0";
            
            return true;
        } else {
            // waiting for the CSS transition to finish
            if (window.requestIdleCallback) {
                requestIdleCallback(function () {
                    loading.style.cssText = "left:100%;";
                    window.scroll(0, 0);
                });
            } else {
                setTimeout(function () {
                    loading.style.cssText = "left:100%;";
                    window.scroll(0, 0);
                }, 250);
            }
            
            return false;
        }
    };
    
    /**
     * Warning: 'hashchange' event fires twice, I implemented a limiter that checks if new hash resembles old one, or if it's new load new page
     * @return {Sniff}
     * */
    this.loadUrl = function () {
        // from a fragment-url, load the appropriate page by comparing the slug to this.routes
        // #fr/le-musee-olympique -> this.routes[le-musee-olympique] ?
        var url = window.location.hash.replace(/[#]/ig, "").split("/"); // [0] = locale, [1] = slug
        var queue = {}; // queue changes and update them
        
        // check if locale has changed
        queue.locale = url[0] !== this.options.locale ? url[0] : this.options.locale;
        
        // load the current slug according to fragment-url's locale | undefined here if slug not found for locale
        var slug = this.routes.current.slug[queue.locale] || undefined;
        
        // if new slug is different from current one
        if (slug && this._slugify(slug).indexOf(url[1]) === -1) {
            
            // plugin integration
            if (this.navigation !== undefined) {
                this.navigation.prepare({locale: this.options.locale, id: this.routes.current.id}).commit();
            }
            
            // hash is new
            for (var i = 0; i < this.routes.length; i++) {
                var current = this.routes[i];
                
                if (this._slugify(current.slug[queue.locale]) === url[1]) {
                    queue.id = current.id;
                    
                    break; // yep, I know I shouldn't use break...
                }
            }
            
            this.update(queue);
            
            return this;
        } else if (url[0] !== this.options.locale) { // it was just a locale change
            
            if (this.navigation !== undefined) {
                this.navigation.prepare({locale: this.options.locale, id: this.routes.current.id}).commit();
            }
            
            queue.locale = url[0];
            
            this.update(queue);
            
            return this;
        } else { // it resembles old one, let's just set the slug back again
            this._updateUrl();
            
            return this;
        }
        
        // slug was not found...
        console.warn("Page could not be found in available pages. Defaulting to current...");
        
        this.fetch(this.options);
        
        return this;
    };
    
    /**
     * Creates a nice fragment-based URL from the data received
     * @return {Sniff}
     * */
    this._updateUrl = function (hash) {
        // should also update page <title>
        if (this.routes === undefined) {
            return console.log("Error, no routes were found.");
        }
        
        var id = this.options.id;
        
        if (hash !== undefined) {
            for (var i = 0; i < this.routes.length; i++) {
                var route = this.routes[i];
                
                if (this._slugify(route.slug[hash.locale]) === hash.slug) {
                    document.title = route.slug[hash.locale] + this.siteName;
                    this.routes["current"] = route;
                    this.options.id = route.id;
                    
                    break;
                }
            }
        }
        
        if (id !== "setup") {
            for (var i = 0; i < this.routes.length; i++) {
                var route = this.routes[i];
                
                if (route.id === id) {
                    document.title = route.slug[this.options.locale] + this.siteName;
                    this.routes["current"] = route;
                    
                    break;
                }
            }
        }
        
        if (this.routes.current !== undefined) {
            // @Lauric: MyCity fixme: check jquery.plugins.js:101 and change the line to var hash = window.location.hash.replace(/[!#\/]/ig, "");
            window.location.hash = this.options.locale + "/" + this._slugify(this.routes.current.slug[this.options.locale]);
        }
        
        return this;
    };
    
    /**
     * placeholder function, needs to be created by user
     * */
    this.onfinish = function () {
        // mainly for jquery plugins as fetch.callbacks works with plain JS
        return;
    };
    
    /* Helper methods */
    
    /**
     * Update specific options (change locale, change id, etc selectively)
     * without re-writing the whole object by passing it to this.fetch();
     *
     * @param {Object} options: set one or more options and update page
     * @return {Object} options
     * */
    this.update = function (options) {
        if (typeof options === "object") {
            for (var option in this.options) {
                if (this.options.hasOwnProperty(option) && options.hasOwnProperty(option)) {
                    if (this._checkIfExists(option, options[option])) {
                        // always better to double-check :]
                        this.options[option] = options[option];
                    }
                }
            }
        } // if something else than object, won't read it and just performs a refresh
        
        this.fetch(this.options);
        
        return this.options;
    };
    
    /**
     * @param {string} key: the key to access in json
     * @param {string} searchable: the value to be found
     * @return {boolean}
     * */
    this._checkIfExists = function (key, searchable) {
        if (typeof this.routes === "undefined" || typeof this.options === "undefined") {
            return console.error("No json nor options found.");
        }
        
        var data = false, options = false;
        
        for (var i = 0; i < this.routes.length; i++) {
            var json = this.routes[i];
            
            if (json[key] === searchable) {
                data = true;
                break;
            }
        }
        
        for (var option in this.options) {
            if (this.options.hasOwnProperty(option) && this.options.hasOwnProperty(key)) {
                options = true;
                break;
            }
        }
        
        return (data || options);
    };
    
    
    /**
     * @param {string} string: the string to be slugified
     * @return {string}
     * */
    this._slugify = function (string) {
        return string.toLowerCase().replace(/[èüùûéöôàäêñç ,.?!]/gi, function (s) {
            return {
                "é": "e",
                "è": "e",
                "ê": "e",
                "ë": "e",
                "à": "a",
                "á": "a",
                "â": "a",
                "ä": "a",
                "î": "i",
                "ö": "o",
                "ô": "o",
                "ç": "c",
                "ñ": "n",
                "û": "u",
                "ù": "u",
                " ": "-",
                ",": "",
                ".": "",
                "?": "",
                "!": ""
            }[s];
        });
    };
    
    /*** PLUGINS ***/
    this.register = function (plugin) {
        if (!this.hasOwnProperty(plugin.name) || this[plugin.name] === undefined) {
            return this[plugin.name] = plugin;
        }
        
        return console.error(plugin.name + " already exists!");
    };
    
    /**
     * Since we only know when a page has been downloaded which routes are available, this
     * method allows querying the server for which routes are available and load the webpage accordingly
     * */
    this.setup = function () {
        if (location.hash !== "") {
            this.options.id = "setup";
            var hash = window.location.hash.replace(/[#]/ig, "").split("/");
            this.routes = routes || {}; // global variable
            this._updateUrl({locale: hash[0], slug: hash[1]});
            this.loadUrl();
        }
    };
    
    /*** INIT ***/
    this.init = function () {
        var sniff = this;
        
        this.setup();
        
        if (this.status.javascript === false) {
            // prevent adding uneeded multiple listeners
            window.addEventListener("hashchange", sniff.loadUrl.bind(sniff), false);
            this.status.javascript = true;
        }
    };
    
    this.init();
}